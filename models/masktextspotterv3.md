# Use MasktextspotterV3 for aicontest

## Environment setup

reference: https://github.com/MhLiao/MaskTextSpotterV3

## Prepare data
1. move public_training_data to
/notebooks/MaskTextSpotterV3_original/datasets/icdar2015/train_images/

### Transform public training data to icdar2015 format
1. Execute trend_to_icdar2015.ipynb under MasktextspotterV3
(output will be under train_gts_reviewed)
2. Move train_gts_reviewed to
/notebooks/MaskTextSpotterV3_original/datasets/icdar2015/train_gts/

### Add generated training data for offline augmentation
1. Execute generate_for_masktextspotter.ipynb under nist_generator
(output will be under generate1_images & generate1_gts)
2. Move files in generate1_images/ to /notebooks/MaskTextSpotterV3_original/datasets/icdar2015/train_images/
3. Move files in generate1_gts/ to /notebooks/MaskTextSpotterV3_original/datasets/icdar2015/train_gts/

## Prepare pretrain weights
1. Download the model pretrain with SynthText (reference: https://github.com/MhLiao/MaskTextSpotterV3)
2. Move the pretrained model to pretrain/pretrain_model.pth

## Training
    pytyhon tools/train_net.py --config-file configs/syndata/seg_rec_poly_fuse_feature.yaml

## Predict
1. move private_data_v2 to /notebooks/private_testing_data/private_data_v2/
2. Execute predict_on_private_test.ipynb.
3. The generated file "submission_conf.csv" contains confidence for ensembling.
