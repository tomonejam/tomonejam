# Tomonejam - PaddleOCR

此模型作為專案的文字辨識器之一 (rbox_rec)，輸入影像會先經過 YORO
Detector 偵測文字進行轉正之後，再使用此模型辨識內文。

## 環境建置

請參閱官方說明：  
<https://github.com/PaddlePaddle/PaddleOCR/blob/release/2.3/doc/doc_en/environment_en.md>

## 資料轉換、準備

訓練此模型之前，需要先將競賽資料轉為 YORO 資料集格式，請參閱 YORO
模型的說明。

## 模型訓練

模型訓練方式可參考官方說明：
<https://github.com/PaddlePaddle/PaddleOCR/blob/release/2.3/doc/doc_en/recognition_en.md>

此專案使用的模型設定檔位於
`./submodules/PaddleOCR/configs/tomonejam_rec`

模型訓練完畢之後須轉換為 Inference Model，詳細請參閱官方說明：  
<https://github.com/PaddlePaddle/PaddleOCR/blob/release/2.3/doc/doc_en/recognition_en.md#Inference>
