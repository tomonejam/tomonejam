# Tomonejam - YORO

此模型作為專案的文字偵測器 (det)，為 bbox_rec (vedastr) 與 rbox_rec
(PaddleOCR) 辨識模型的前處理器。

## 環境建置

請參閱官方說明：<https://github.com/jamesljlster/yoro#requirement>

## 資料轉換、準備

請搭配此專案在 `data_process` 資料夾提供的腳本將競賽資料轉換為 YORO
格式。

## 模型訓練

模型訓練方式可參考 <https://github.com/jamesljlster/yoro-tutorial>，
此專案使用的模型訓練配置檔為
`./submodules/yoro/config/tomonejam_yolov4-tiny.yaml`。
