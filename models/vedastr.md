# Use Vedastr for aicontest

## Environment setup
reference: https://github.com/Media-Smart/vedastr

## Train Pretrained
python tools/train.py configs/tps_resnet_bilstm_attn_pretrain.py "0, 1, 2, 3"

## Prepare data
1. use data_processing/binarize.ipynb to generate synthetic data
2. use data_processing/crop_data_for_recognizer.ipynb to crop training data for recognizer
3. move the output data of step 1. & 2. to data/tbrain/

## Train
1. specify pre-trained weights in configs/tps_resnet_bilstm_folder.py
	e.g., resume={'checkpoint':'workdir/tps_resnet_bilstm_attn_pretrain/best_norm.pth'}
2. python tools/train.py configs/tps_resnet_bilstm_folder.py "0, 1, 2, 3" 

## Predict
1. use data_processing/crop_data_for_recognizer.ipynb to crop testing data for recognizer
2. python tools/inference_metric.py configs/tps_resnet_bilstm_folder.py checkpoint_path img_path "0"