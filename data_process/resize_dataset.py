import yaml
import numpy as np
import cv2 as cv

from os import makedirs
from os.path import join, splitext, basename

from glob import glob
from shutil import copy2
from tqdm import tqdm

anno_min_dim = 416. / 32
img_max_dim = 512

#proc_list = [
#    ('./public_training_data', './rbox/train_resize'),
#    ('./public_validation_data', './rbox/valid_resize')
#]

proc_list = [
    ('./public_total_data', './rbox/total_resize')
]

for (in_dir, out_dir) in proc_list:

    makedirs(out_dir)
    anno_list = glob(join(in_dir, '*.mark'))
    for anno_path in tqdm(anno_list):

        # Check image resolution
        img_path = splitext(anno_path)[0]
        img = cv.imread(img_path)
        imgSize = np.array(img.shape[:2])
        if np.max(imgSize) <= img_max_dim:
            copy2(img_path, out_dir)
            copy2(anno_path, out_dir)
            continue

        # Resize image
        scale = img_max_dim / np.max(imgSize)
        img = cv.resize(img, (0, 0), fx=scale, fy=scale)
        cv.imwrite(join(out_dir, basename(img_path)), img)

        # Resize annotation
        with open(anno_path, 'r') as f:
            newAnno = []
            anno = yaml.load(f, Loader=yaml.FullLoader)
            for inst in anno:

                inst['x'] = float(inst['x'] * scale)
                inst['y'] = float(inst['y'] * scale)
                inst['w'] = float(inst['w'] * scale)
                inst['h'] = float(inst['h'] * scale)

                newAnno.append(inst)
                """
                if min(inst['w'], inst['h']) > anno_min_dim:
                    newAnno.append(inst)
                """

        with open(join(out_dir, basename(anno_path)), 'w') as f:
            yaml.dump(newAnno, f)
