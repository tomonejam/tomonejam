import json

from os import makedirs
from os.path import join, exists

from shutil import copy2
from tqdm import tqdm


if __name__ == '__main__':

    reviewPath = './public_training_data_review.json'
    imageDir = './public_training_data'
    outputDir = './public_training_data_resort'
    normalDir = join(outputDir, 'normal')
    reverseDir = join(outputDir, 'reverse')

    # Open review data
    with open(reviewPath, 'r') as f:
        reviewData = json.load(f)
    attr = reviewData['attr']

    # Create output dir
    makedirs(normalDir)
    makedirs(reverseDir)

    # Copy images
    for fileName in tqdm(attr.keys()):

        imgPath = join(imageDir, fileName)
        markPath = imgPath + '.mark'
        if not exists(markPath):
            markPath = None

        destDir = reverseDir if attr[fileName]['reverse'] else normalDir
        copy2(imgPath, destDir)
        if markPath:
            copy2(markPath, destDir)
