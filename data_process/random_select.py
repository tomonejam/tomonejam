import argparse

from tqdm import tqdm
from glob import glob
from random import shuffle
from shutil import copy2

from os import makedirs
from os.path import join, exists


if __name__ == '__main__':

    # Parse arguments
    argp = argparse.ArgumentParser(description='Random split dataset')

    argp.add_argument('source', type=str, help='Source directory')
    argp.add_argument('select_ratio', type=float, help='Select ratio')
    argp.add_argument('output_dir', type=str, help='Output directory')
    argp.add_argument('left_dir', type=str, help='Left directory')

    args = argp.parse_args()

    # Random select
    makedirs(args.output_dir, exist_ok=True)
    makedirs(args.left_dir, exist_ok=True)

    samples = list(glob(join(args.source, '*.jpg')))
    shuffle(samples)

    splitPos = int(len(samples) * args.select_ratio)
    select = samples[:splitPos]
    left = samples[splitPos:]

    def list_copy2(file_list, output_dir):
        for file_path in tqdm(file_list):
            copy2(file_path, output_dir)
            markPath = file_path + '.mark'
            if exists(markPath):
                copy2(markPath, output_dir)

    list_copy2(select, args.output_dir)
    list_copy2(left, args.left_dir)
