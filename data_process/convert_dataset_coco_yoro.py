import yaml

import numpy as np
import cv2 as cv

from pycocotools.coco import COCO

from glob import glob
from tqdm import tqdm
from os.path import dirname, basename, splitext, join


if __name__ == '__main__':

    annoPath = '/home/james/Desktop/Tomonejam/Integration/public_testing_anno_working/public_testing_unconsensus_reviewed.json'
    imgDir = '/home/james/Desktop/Tomonejam/Integration/public_testing_anno_working/unconsensus_samples'
    #imgDir = join(dirname(annoPath), splitext(basename(annoPath))[0])

    # Load annotations
    coco = COCO(annoPath)

    # Convert annotations
    for imgId in tqdm(coco.imgs):

        fname = coco.loadImgs(imgId)[0]['file_name']
        fpath = join(imgDir, fname)

        anno = []
        anns = coco.loadAnns(coco.getAnnIds(imgIds=imgId))
        for ann in anns:

            vertices = np.array(ann['keypoints']).reshape(-1, 3)[..., 0:2]
            topRigit = vertices[0]
            btmRight = vertices[1]
            btmLeft = vertices[2]
            topLeft = vertices[3]
            center = np.mean(vertices, axis=0)

            ctrLeft = (topLeft + btmLeft) / 2
            ctrRight = (topRigit + btmRight) / 2
            ctrDiff = ctrRight - ctrLeft
            rad = np.arctan2(ctrDiff[1], ctrDiff[0])

            rotMat = np.array([
                [np.cos(rad), -np.sin(rad)],
                [np.sin(rad), np.cos(rad)],
            ])

            poly = np.matmul(vertices - center, rotMat)
            width = np.max(poly[..., 0] - np.min(poly[..., 0]))
            height = np.max(poly[..., 1] - np.min(poly[..., 1]))

            anno.append({
                'label': 0,
                'x': float(center[0]),
                'y': float(center[1]),
                'w': float(width),
                'h': float(height),
                'degree': float(-rad * 180 / np.pi)
            })

        with open(fpath + '.mark', 'w') as f:
            f.write(yaml.dump(anno))
