# Data Processing 相關程式

## 檔案簡介

-   `review_dataset.py`：一個快速標記影像方向性的簡易工具。
-   `convert_dataset.py`：將競賽資料轉為 COCO Keypoint Dataset 格式。
-   `convert_dataset_coco_yoro.py`：將 COCO 資料集轉為 YORO 格式。
-   `split_dataset_normal_reverse.py`：將競賽資料依據方向標記結果切分為
    normal/reverse 資料夾。
-   `random_select.py`：將 YORO 資料集進行亂數切分。
-   `resize_dataset.py`：將 YORO 資料集進行 Resize 處理。
