import json

import numpy as np
import cv2 as cv
import pandas as pd

from os.path import basename, dirname, splitext, join, exists

from tqdm import tqdm

cats = [{
    "id": 1,
    "name": "label",
    "supercategory": "label",
    "keypoints": ["top_right", "bottom_right", "bottom_left", "top_left"],
    "skeleton": [
        [1, 2],
        [1, 4],
        [2, 3],
        [3, 4]
    ]
}]


def gen_roi_anno(data):

    # Generate segmentations, bounding boxes and keypoints from annotations
    kptsList = []
    segmList = []
    bboxList = []
    for trX, trY, brX, brY, blX, blY, tlX, tlY in zip(
        data['top right x'], data['top right y'],
        data['bottom right x'], data['bottom right y'],
        data['bottom left x'], data['bottom left y'],
        data['top left x'], data['top left y']
    ):
        kpts = [trX, trY, 2, brX, brY, 2, blX, blY, 2, tlX, tlY, 2]
        segm = np.array(kpts).reshape(-1, 3)[..., 0:2].astype(np.float32)
        bbox = list(cv.boundingRect(segm))
        area = cv.contourArea(segm)
        segm = [segm.reshape(-1).tolist()]

        kptsList.append(kpts)
        segmList.append((segm, area))
        bboxList.append(bbox)

    return bboxList, segmList, kptsList


if __name__ == '__main__':

    anno_list = ['./public_validation_data_by_retrained.csv']
    imgdir_list = ['./public_validation_data']

    anno_list = [
        '/home/james/Desktop/Tomonejam/Integration/public_testing_anno_working/public_testing_unconsensus_reviewed.csv']
    imgdir_list = [
        '/home/james/Desktop/Tomonejam/Integration/public_testing_anno_working/unconsensus_samples']

    for annoPath, imgDir in zip(anno_list, imgdir_list):

        # Parse annotations
        data = pd.read_csv(annoPath)
        fnList = data['filename']
        textList = data['label']
        if 'top right x' in data:
            bboxList, segmList, kptsList = gen_roi_anno(data)
        else:
            bboxList, segmList, kptsList = None, None, None

        reverseAttrs = list(
            data['reverse']) if 'reverse' in data else None
        handWriteAttrs = list(
            data['hand_write']) if 'hand_write' in data else None
        unrecogAttrs = list(
            data['unrecognizable']) if 'unrecognizable' in data else None

        reviewPath = splitext(annoPath)[0] + '_review.json'
        reviewData = {}
        if exists(reviewPath):
            print('Loading review data from:', reviewPath)
            with open(reviewPath, 'r') as f:
                reviewData = json.load(f)

        attr = reviewData.get('attr', {})

        # Generate COCO annotations
        images = []
        annotations = []

        counter = 1
        for idx in tqdm(range(len(fnList)), desc='Processing %s' % basename(annoPath)):

            imgId = counter
            annoId = counter
            counter += 1

            # Read image and construct information
            fileName = fnList[idx] + '.jpg'
            img = cv.imread(join(imgDir, fileName))
            height, width, _ = img.shape

            reverse = attr.get(fileName, {}).get('reverse', False)
            if reverseAttrs:
                reverse = reverse or reverseAttrs[idx]

            images.append({
                'id': imgId,
                'width': width,
                'height': height,
                'file_name': fileName
            })

            # Construct annotations
            text = textList[idx]
            if not isinstance(text, str):
                text = ''
            anno = {
                'id': annoId,
                'image_id': imgId,
                'category_id': 1,
                'text': text
            }

            if bboxList:
                anno['bbox'] = bboxList[idx]
            if segmList:
                segm, area = segmList[idx]
                anno['segmentation'] = segm
                anno['area'] = area
                anno['iscrowd'] = False
            if kptsList:

                kpts = kptsList[idx]
                if reverse:
                    kpts = np.array(kpts).reshape(-1, 3)
                    kpts = np.roll(kpts, 2, axis=0)
                    kpts = kpts.reshape(-1).tolist()

                anno['keypoints'] = kpts
                anno['num_keypoints'] = 4
            if reverseAttrs:
                anno['reverse'] = reverse
            if handWriteAttrs:
                anno['hand_write'] = handWriteAttrs[idx]
            if unrecogAttrs:
                anno['unrecognizable'] = unrecogAttrs[idx]

            annotations.append(anno)

        jsonPath = splitext(annoPath)[0] + '.json'
        with open(jsonPath, 'w') as f:
            json.dump({
                'categories': cats,
                'images': images,
                'annotations': annotations
            }, f)
