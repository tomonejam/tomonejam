import json

import numpy as np
import cv2 as cv
import pandas as pd

from os.path import exists, basename, dirname, splitext, join

from collections import defaultdict
from copy import deepcopy

from convert_dataset import gen_roi_anno

anno_file = './public_training_data.csv'
img_dir = join(dirname(anno_file), splitext(basename(anno_file))[0])
review_data_path = splitext(basename(anno_file))[0] + '_review.json'

attr_template = {
    'reverse': False,
    'mislabeled': False
}

if __name__ == '__main__':

    reviewData = {}
    if exists(review_data_path):
        with open(review_data_path, 'r') as f:
            reviewData = json.load(f)

    data = pd.read_csv(anno_file)
    fnList = data['filename']
    labelList = data['label']
    if len(data.keys()) > 2:
        bboxList, segmList, kptsList = gen_roi_anno(data)
    else:
        bboxList, segmList, kptsList = None, None, None

    idx = reviewData.get('progress', 0)
    attr = reviewData.get('attr', {})
    total = len(fnList)

    try:
        while True:

            print('Reviewing %d/%d' % (idx + 1, total))

            fileName = fnList[idx] + '.jpg'
            label = labelList[idx]
            attr[fileName] = {
                **attr_template,
                **attr.get(fileName, {})
            }

            while True:

                image = cv.imread(join(img_dir, fileName))
                assert image is not None

                reverse = 'Reverse: %s' % (attr[fileName]['reverse'])
                mislabel = 'Mislabeled: %s' % (attr[fileName]['mislabeled'])
                text = 'Label: %s' % label
                progress = 'Progress: %.2f %% (%d/%d)' % (
                    len(attr) * 100 / len(fnList), len(attr), len(fnList))

                fontFace = cv.FONT_HERSHEY_SIMPLEX
                fontScale = 1.0
                thickness = 3

                labelPos = (50, 150)
                if bboxList:
                    text = label
                    (x, y, w, h) = bboxList[idx]
                    (textW, textH), _ = cv.getTextSize(
                        text, fontFace, fontScale, thickness)
                    labelPos = (int(x), int(y + h + textH + 10))
                    cv.rectangle(image, (x, y), (x + w, y + h),
                                 (128, 128, 128), 1, cv.LINE_AA)

                (textW, textH), _ = cv.getTextSize(
                    progress, fontFace, fontScale, thickness)
                cv.putText(
                    image, progress, (image.shape[1] - textW, textH),
                    fontFace, fontScale, (255, 255, 255), thickness, cv.LINE_AA)

                color = (128, 128, 255) \
                    if attr[fileName]['reverse'] else (255, 128, 128)
                cv.putText(image, reverse, (50, 50), fontFace, fontScale,
                           color, thickness, cv.LINE_AA)

                color = (0, 170, 255) \
                    if attr[fileName]['mislabeled'] else (170, 255, 127)
                cv.putText(image, mislabel, (50, 100), fontFace, fontScale,
                           color, thickness, cv.LINE_AA)

                cv.putText(image, text, labelPos, fontFace, fontScale,
                           (255, 255, 255), thickness, cv.LINE_AA)

                cv.imshow('Reviewing', image)
                kbin = cv.waitKey(0)
                if kbin == 27:
                    break

                try:
                    kbin = chr(kbin).upper()
                except:
                    pass

                if kbin == 'R':
                    attr[fileName]['reverse'] = (not attr[fileName]['reverse'])
                elif kbin == 'M':
                    attr[fileName]['mislabeled'] = (
                        not attr[fileName]['mislabeled'])
                elif kbin in ['N', 'P', 'J', 'K']:
                    break

            if kbin in ['N', 'J']:
                idx += 1
            elif kbin in ['P', 'K']:
                idx -= 1
            elif kbin == 27:
                break

            idx = idx % total

    except Exception as e:
        print('Exception received:')
        print(e)
        print()

    print('Saving review data')
    reviewData['progress'] = idx
    reviewData['attr'] = attr
    with open(review_data_path, 'w') as f:
        json.dump(reviewData, f)
