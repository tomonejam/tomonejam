# === Configure runtime path === #
import sys
from os.path import abspath, join, dirname

sys.path.append(abspath(join(dirname(__file__), "../submodules/masktextspotterv3")))
# ============================== #

import argparse

import numpy as np
import cv2 as cv

from glob import glob
from tqdm import tqdm

from textspotter import TEXTSPOTTER
from maskrcnn_benchmark.config import cfg
from pathlib import Path


def PolyArea(x, y):
    return 0.5 * np.abs(np.dot(x, np.roll(y, 1)) - np.dot(y, np.roll(x, 1)))


if __name__ == "__main__":

    # Parse arguments
    argp = argparse.ArgumentParser(description="e2e_rec")

    argp.add_argument(
        "--imgdir", type=str, required=True, help="Image folder for inference"
    )
    argp.add_argument("--config", type=str, required=True, help="Model config file")
    argp.add_argument("--weights", type=str, required=True, help="Model weights file")
    argp.add_argument("--out-path", type=str, required=True, help="Output path")

    args = argp.parse_args()

    # Load model
    config = cfg.merge_from_file(args.config)
    min_size = 1000

    predictor = TEXTSPOTTER(
        cfg,
        cambrian_config=config,
        weight_path=args.weights,
        min_image_size=min_size,
        confidence_threshold=0.7,
        output_polygon=True,
    )

    # Inference for *.jpg in imgdir
    paths = glob(join(args.imgdir, "*.jpg"))
    with open(args.out_path, "w") as f:
        f.write("id,text,conf")
        for p in tqdm(paths):
            img = cv.imread(p)
            results = predictor.predict_for_cambrian(img)
            idf = Path(p).stem
            maxarea = 0
            maxid = -1
            for i, result in enumerate(results):
                poly = np.array(result["value"]).reshape(-1, 2)
                x = poly[:, 0]
                y = poly[:, 1]
                area = PolyArea(x, y)
                if area > maxarea:
                    maxarea = area
                    maxid = i
            text = "0"
            conf = 0
            if maxid != -1:
                text = results[maxid]["text"].upper()
                if "O" in text:
                    print("O in", text)
                text = text.replace("O", "0")
                conf = results[maxid]["conf"]
            f.write("\r\n{},{},{}".format(idf, text, str(conf)))
