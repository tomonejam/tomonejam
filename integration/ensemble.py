import argparse
import pprint

import numpy as np
import pandas as pd

from collections import defaultdict
from os.path import join, basename, splitext
from glob import glob


if __name__ == "__main__":

    # Parse arguments
    argp = argparse.ArgumentParser(description="bbox_rec")

    argp.add_argument(
        "--imgdir", type=str, required=True, help="Image folder for inference"
    )
    argp.add_argument(
        "--in-csv",
        action="append",
        nargs=2,
        metavar=("file", "conf_mul"),
        required=True,
        help="Input csv path and confidence multiplier",
    )

    argp.add_argument("--out-path", required=True, help="Output path")

    args = argp.parse_args()

    src_data = [
        (pd.read_csv(path, index_col="id"), float(scale))
        for (path, scale) in args.in_csv
    ]

    out_ids = []
    out_texts = []
    stat = defaultdict(int)
    for img_path in glob(join(args.imgdir, "*.jpg")):
        img_name = splitext(basename(img_path))[0]

        conf_list = np.array(
            [
                src.at[img_name, "conf"] * conf if img_name in src.index else 0
                for (src, conf) in src_data
            ]
        )

        text_list = np.array(
            [
                src.at[img_name, "text"] if img_name in src.index else "0"
                for (src, conf) in src_data
            ]
        )

        sort_ind = np.argsort(conf_list)
        conf_list = conf_list[sort_ind]
        text_list = text_list[sort_ind]

        result = None
        if len(set(text_list)) == len(src_data):
            result = text_list[-1]
            stat["max_conf"] += 1
        else:
            text_list = [
                text
                for (idx, text) in enumerate(text_list)
                if (text != "0" or conf_list[idx] > 0)
            ]
            if len(text_list) > 0:
                result = max(text_list, key=text_list.count)

            if len(set(text_list)) == 1:
                stat["consist"] += 1
            else:
                stat["majority"] += 1

        if result is not None:
            out_ids.append(img_name)
            out_texts.append(result)
        else:
            stat["empty"] += 1

    pd.DataFrame({"id": out_ids, "text": out_texts}).to_csv(args.out_path, index=False)

    print("Stat:")
    pp = pprint.PrettyPrinter(indent=2)
    pp.pprint(dict(stat))
