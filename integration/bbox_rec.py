# === Configure runtime path === #
import sys
from os.path import abspath, join, dirname

sys.path.append(abspath(join(dirname(__file__), "../submodules/vedastr")))
# ============================== #

import argparse
import csv
import math
import yaml

import numpy as np
import cv2 as cv

from os import makedirs
from os.path import basename, exists, splitext

from glob import glob
from tqdm import tqdm

from vedastr.runners import InferenceRunner
from vedastr.utils import Config


def main():

    # Parse arguments
    argp = argparse.ArgumentParser(description="rbox_rec")

    argp.add_argument(
        "--imgdir", type=str, required=True, help="Image path for inference"
    )
    argp.add_argument(
        "--rboxdir", type=str, required=True, help="Directory path for rbox information"
    )
    argp.add_argument(
        "--cropdir", type=str, required=True, help="Cropped image directory"
    )
    argp.add_argument("--config", type=str, required=True, help="Config file path")
    argp.add_argument(
        "--checkpoint", type=str, required=True, help="Checkpoint file path"
    )
    argp.add_argument("--gpus", type=str, default="0", help="target gpus")
    argp.add_argument("--out-path", type=str, required=True, help="Output path")

    args = argp.parse_args()

    # Load config
    cfg_path = args.config
    cfg = Config.fromfile(cfg_path)

    deploy_cfg = cfg["deploy"]
    common_cfg = cfg.get("common")
    deploy_cfg["gpu_id"] = args.gpus.replace(" ", "")

    # Crop image
    # We need to reproduce the inference result, so this step becomes necessary
    makedirs(args.cropdir, exist_ok=True)
    for img in tqdm(glob(join(args.imgdir, "*.jpg")), desc="Cropping image"):

        filename = basename(img)

        # Load rbox information
        rbox_path = join(args.rboxdir, filename + ".mark")
        if not exists(rbox_path):
            print("RBox information is not found for", filename)
            continue

        with open(rbox_path, "r") as f:
            rbox_info = yaml.load(f, Loader=yaml.FullLoader)[0]

        # Load and crop image by rbox information
        image = cv.imread(img)
        image = crop_image(image, rbox_info)
        cv.imwrite(join(args.cropdir, filename), image)

    # Load model
    runner = InferenceRunner(deploy_cfg, common_cfg)
    runner.load_checkpoint(args.checkpoint)

    # Inference for cropped image folder
    with open(args.out_path, "w", newline="") as outcsv:

        writer = csv.writer(outcsv)
        writer.writerow(["id", "text", "conf"])

        for img in tqdm(glob(join(args.cropdir, "*.jpg"))):

            filename = basename(img)

            # Load and crop image by rbox information
            image = cv.imread(img)
            image = cv.cvtColor(image, cv.COLOR_BGR2RGB)

            # Inference
            pred_str, probs = runner(image)
            writer.writerow(
                [splitext(filename)[0], pred_str[0].upper(), str(float(probs[0]))]
            )


def crop_image(image, rbox):

    height, width, channels = image.shape

    # Convert rbox to points
    th = rbox["degree"]
    x = rbox["x"]
    y = rbox["y"]
    w = rbox["w"]
    h = rbox["h"]

    if math.cos(math.radians(th)) < 0:
        th = th + 180
    rad = math.radians(th)

    sinVal = math.sin(rad)
    cosVal = math.cos(rad)
    rotMat = np.float32([[cosVal, -sinVal], [sinVal, cosVal]])

    origMat = np.float32([x, y])

    pts = np.zeros((4, 2), dtype=np.float32)
    pts[0] = np.matmul(np.float32([+w / 2, -h / 2]), rotMat) + origMat
    pts[1] = np.matmul(np.float32([+w / 2, +h / 2]), rotMat) + origMat
    pts[2] = np.matmul(np.float32([-w / 2, +h / 2]), rotMat) + origMat
    pts[3] = np.matmul(np.float32([-w / 2, -h / 2]), rotMat) + origMat

    # Crop image
    y_top = max(0, int(np.min(pts[:, 1])))
    y_bottom = min(height, int(np.max(pts[:, 1])))
    x_left = max(0, int(np.min(pts[:, 0])))
    x_right = min(width, int(np.max(pts[:, 0])))

    image = image[y_top:y_bottom, x_left:x_right].copy()

    return image


if __name__ == "__main__":
    main()
