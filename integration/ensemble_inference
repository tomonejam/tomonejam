#!/usr/bin/python

import argparse
from subprocess import run

from os import makedirs
from os.path import dirname, join, exists

if __name__ == "__main__":

    # Parse arguments
    argp = argparse.ArgumentParser(description="Ensemble inference")

    argp.add_argument(
        "--imgdir", type=str, required=True, help="Image folder path for inference"
    )
    argp.add_argument("--modeldir", type=str, required=True, help="Model folder path")
    argp.add_argument(
        "--workdir", type=str, required=True, help="Working directory path"
    )

    args = argp.parse_args()

    # Cache variables
    execdir = dirname(__file__)
    imgdir = args.imgdir
    modeldir = args.modeldir
    workdir = args.workdir

    makedirs(workdir, exist_ok=True)

    # Check for all model files
    def _check_path(path):
        if not exists(path):
            raise FileNotFoundError(f"Required file is not found: {path}")
        return path

    e2e_config = _check_path(join(modeldir, "e2e", "e2e.yaml"))
    e2e_weights = _check_path(join(modeldir, "e2e", "e2e.pth"))

    det_model = _check_path(join(modeldir, "det.zip"))

    rbox_rec_dir = _check_path(join(modeldir, "rbox_rec"))
    rbox_rec_char_dict = _check_path(join(rbox_rec_dir, "char_dict.txt"))
    rbox_rec_model = _check_path(join(rbox_rec_dir, "inference"))
    rbox_rec_model_pdi_param = _check_path(join(rbox_rec_model, "inference.pdiparams"))
    rbox_rec_model_pdi_info = _check_path(
        join(rbox_rec_model, "inference.pdiparams.info")
    )
    rbox_rec_model_pd = _check_path(join(rbox_rec_model, "inference.pdiparams.info"))

    bbox_rec_config = _check_path(join(modeldir, "bbox_rec", "bbox_rec.py"))
    bbox_rec_weights = _check_path(join(modeldir, "bbox_rec", "bbox_rec.pth"))

    # Ensemble inference
    run(
        [
            # fmt: off
            "python", join(execdir, "e2e_rec.py"),
            "--imgdir", imgdir,
            "--config", e2e_config,
            "--weights", e2e_weights,
            "--out-path", join(workdir, "e2e_rec.csv"),
        ],
        check=True,
    )

    run(
        [
            # fmt: off
            "python", join(execdir, "rbox_rec.py"),
            "--imgdir", imgdir,
            "--det-model", det_model,
            "--rec-model", rbox_rec_model,
            "--out-path", join(workdir, "rbox_rec.csv"),
            "--char-dict", rbox_rec_char_dict,
            "--record-rbox-path", join(workdir, "rbox_info"),
            "--record-conf",
        ],
        check=True,
    )

    run(
        [
            # fmt: off
            "python", join(execdir, "bbox_rec.py"),
            "--imgdir", imgdir,
            "--rboxdir", join(workdir, "rbox_info"),
            "--cropdir", join(workdir, "cropdir"),
            "--config", bbox_rec_config,
            "--checkpoint", bbox_rec_weights,
            "--out-path", join(workdir, "bbox_rec.csv"),
        ],
        check=True,
    )

    run(
        [
            # fmt: off
            "python", join(execdir, "ensemble.py"),
            "--imgdir", imgdir,
            "--out-path", join(workdir, "ensemble.csv"),
            "--in-csv", join(workdir, "e2e_rec.csv"), "0.88",
            "--in-csv", join(workdir, "rbox_rec.csv"), "1.00",
            "--in-csv", join(workdir, "bbox_rec.csv"), "0.90",
        ],
        check=True,
    )
