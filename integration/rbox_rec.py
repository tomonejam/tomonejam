# === Configure runtime path === #
import sys
from os.path import abspath, join, dirname

sys.path.append(abspath(join(dirname(__file__), "../submodules/yoro")))
sys.path.append(abspath(join(dirname(__file__), "../submodules/PaddleOCR")))
# ============================== #

import argparse
import math
import yaml

import numpy as np
import cv2 as cv
import pandas as pd

from os import makedirs
from os.path import basename, splitext
from argparse import Namespace
from glob import glob

from yoro.api import YORODetector
from tools.infer.predict_rec import TextRecognizer


if __name__ == "__main__":

    # Parse arguments
    argp = argparse.ArgumentParser(description="rbox_rec")

    argp.add_argument(
        "--imgdir", type=str, required=True, help="Image path for inference"
    )
    argp.add_argument(
        "--det-model", type=str, required=True, help="Detector model path"
    )
    argp.add_argument(
        "--rec-model", type=str, required=True, help="Recognizer model path"
    )
    argp.add_argument("--out-path", type=str, required=True, help="Output path")

    det_opt = argp.add_argument_group("Detector optional arguments")
    det_opt.add_argument(
        "--conf-scan",
        type=float,
        nargs="+",
        default=[0.7, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1],
        help="Confidence scan for detector",
    )
    det_opt.add_argument("--nms", type=float, default=0.6, help="NMS threshold")
    det_opt.add_argument(
        "--bbox-resize-ratio",
        type=float,
        default=1.2,
        help="Resize ratio for bounding box",
    )

    rec_opt = argp.add_argument_group("Recognizer optional arguments")
    rec_opt.add_argument(
        "--char-dict",
        type=str,
        default="ppocr_word_dict.txt",
        help="Character list for recognizer",
    )
    rec_opt.add_argument(
        "--rec-image-shape",
        type=str,
        default="3,32,320",
        help="Image shape for recognizer",
    )

    gen_opt = argp.add_argument_group("Other optional arguments")
    gen_opt.add_argument("--record-conf", action="store_true", help="Record confidence")
    gen_opt.add_argument(
        "--record-rbox-path", type=str, default=None, help="Save detection result"
    )

    args = argp.parse_args()

    det_model_path = args.det_model
    conf_scan = args.conf_scan
    nms_th = args.nms
    bbox_resize_ratio = args.bbox_resize_ratio

    rec_model_dir = args.rec_model
    rec_char_dict = args.char_dict
    rec_image_shape = args.rec_image_shape

    image_dir = args.imgdir
    out_path = args.out_path

    if args.record_rbox_path is not None:
        makedirs(args.record_rbox_path, exist_ok=True)

    # Load detector model
    detector = YORODetector(det_model_path)

    # Load recognition model
    recognizer = TextRecognizer(
        Namespace(
            rec_char_dict_path=rec_char_dict,
            rec_model_dir=rec_model_dir,
            rec_image_shape=rec_image_shape,
            rec_char_type="ch",
            rec_batch_num=1,
            rec_algorithm="CRNN",
            use_space_char=True,
            use_gpu=True,
            gpu_mem=500,
            use_tensorrt=False,
            benchmark=False,
        )
    )

    # Inference
    res_id = []
    res_text = []
    res_conf = []

    image_list = glob(join(image_dir, "*.jpg"))
    for idx, image_path in enumerate(image_list):

        print("Processing %d/%d: %s" % (idx + 1, len(image_list), image_path))
        image_id = splitext(basename(image_path))[0]

        # Detection
        image = cv.imread(image_path, cv.IMREAD_COLOR)
        rbox_list = []
        for conf_th in conf_scan:

            print(f"Using conf_th = {conf_th}")
            rbox_list = detector.detect(image, conf_th, nms_th)
            if len(rbox_list) > 0:
                break

        if len(rbox_list) == 0:
            print("Empty detection!")
            print()
            continue

        # Recognition for all rotated bounding boxes
        rec_conf = []
        rec_text = []
        for det in rbox_list:

            image_proc = image.copy()

            # Cropping with rotated bounding box
            ctr_x = det.x
            ctr_y = det.y
            box_w = det.w * bbox_resize_ratio
            box_h = det.h * bbox_resize_ratio
            rad = math.radians(det.degree)

            img_w = image_proc.shape[1]
            img_h = image_proc.shape[0]

            matrix = cv.invertAffineTransform(
                np.float32(
                    [
                        [
                            math.cos(rad),
                            math.sin(rad),
                            -ctr_x * math.cos(rad) - ctr_y * math.sin(rad) + ctr_x,
                        ],
                        [
                            -math.sin(rad),
                            math.cos(rad),
                            -ctr_y * math.cos(rad) + ctr_x * math.sin(rad) + ctr_y,
                        ],
                    ]
                )
            )

            image_proc = cv.warpAffine(
                image_proc, matrix, (image_proc.shape[1], image_proc.shape[0])
            )
            image_proc = image_proc[
                max(int(ctr_y - box_h / 2), 0) : min(int(ctr_y + box_h / 2), img_h),
                max(int(ctr_x - box_w / 2), 0) : min(int(ctr_x + box_w / 2), img_w),
                :,
            ]

            # Text recognition
            rec_res, _ = recognizer([image_proc])
            rec_conf.append(rec_res[0][1] * det.conf)
            rec_text.append(rec_res[0][0])

        # Record most confident result
        conf = np.max(rec_conf)
        text = rec_text[np.argmax(rec_conf)]
        print("Conf:", conf)
        print("Text:", text)

        res_id.append(image_id)
        res_conf.append(conf)

        res_idx = np.argmax(rec_conf)
        res_text.append(rec_text[res_idx])

        if args.record_rbox_path is not None:
            anno_path = join(args.record_rbox_path, basename(image_path + ".mark"))
            print("Save detection result to:", anno_path)
            with open(anno_path, "w") as f:
                f.write(yaml.dump([rbox_list[res_idx].to_dict()]))

        print()

    data_dict = {
        "id": res_id,
        "text": res_text,
    }

    if args.record_conf:
        print("Confidence recording is enabled")
        data_dict["conf"] = res_conf

    data = pd.DataFrame(data_dict)
    data.to_csv(out_path, index=False)
