# Tomonejam

An ensemble model for:  
中鋼人工智慧挑戰賽-字元辨識
<https://tbrain.trendmicro.com.tw/Competitions/Details/17>

## 此專案使用的開源模型

-   [MaskTextSpotterV3](https://github.com/MhLiao/MaskTextSpotterV3)

    MaskTextSpotterV3 為此專案系統使用的 End-to-End 辨識模型，在此
    Readme 後續會以 e2e 代稱之。

-   [YORO](https://github.com/jamesljlster/yoro)

    YORO 為此專案系統使用的 Detection 偵測模型，在此 Readme 後續會以 det
    代稱之。

-   [vedastr](https://github.com/Media-Smart/vedastr)

    vedastr 為此專案系統使用的 Recognition 辨識模型之一，在此 Readme
    後續會以 bbox_rec 代稱之。

-   [PaddleOCR](https://github.com/PaddlePaddle/PaddleOCR)

    PaddleOCR 為此專案系統使用的 Recognition 辨識模型之一，在此 Readme
    後續會以 rbox_rec 代稱之。

為 Tomonejam 專案修改的 Fork 皆位於 [submodules](./submodules)
資料夾中，各項 Model 的訓練方式與說明請參閱 [models](./models) 資料夾。

## 運作流程

1.  End-to-End Recognition

    利用 MaskTextSpotterV3 對輸入影像進行 End-to-End
    辨識得到輸出字串列表，取信心分數最高的字串為 `e2e_result`
    與對應的信心分數 `e2e_conf`。

2.  利用 YORO 對輸入影像進行文字偵測，得到含有角度資訊的旋轉物件框列表
    (Rotated Bounding Boxes, det_rboxes)。

3.  利用 PaddleOCR Recognition
    模型對每一個偵測到的旋轉物件框進行辨識，取偵測、辨識信心分數相乘最高的組合為：

    -   `det_rbox`：信心分數相乘最高的旋轉物件框。
    -   `rbox_rec_result`：信心分數最高的辨識結果字串。
    -   `rbox_rec_conf`：偵測、辨識信心分數相乘結果。

4.  對 `det_rbox` 取最小外接矩形為 `det_bbox`，並利用 vedastr
    Recognition 模型對此範圍之影像進行辨識， 得到辨識結果
    `bbox_rec_result` 與信心分數 `bbox_rec_conf`。

5.  對 `e2e_result`、`rbox_rec_result` 以及 `bbox_rec_result`
    進行比較，若有兩個以上的字串為相同的，取該字串作為最終辨識結果；  
    如果三者皆不同，則對 `e2e_conf` \* 0.88，`rbox_rec_conf` \*
    1.00，`bbox_rec_conf` \*
    0.90，並取最高分數對應的字串為最終辨識結果。

## 此專案額外使用的公開資料

-   NIST (by-class)：<https://www.nist.gov/srd/nist-special-database-19>

## 硬體環境限制

此專案提供之 Docker Image 為基於 NVIDIA RTX 2080
所屬的硬體架構之上建構， 不相容於 sm_75 架構的 NVIDIA GPU 會無法使用。

## 環境建置

請建立一相容於上述所有開源模型的環境，或直接使用此 Docker Image：

``` bash
docker pull zhengling/tomonejam:base
```

如果不能連上網路，請嘗試清除 proxy 相關環境變數：

``` bash
unset HTTPS_PROXY HTTP_PROXY https_proxy http_proxy
```

一旦環境建置完畢，請使用下列指令進行 recursive clone 並完成專案建置。

``` bash
git clone --recursive https://gitlab.com/tomonejam/tomonejam.git
cd tomonejam/

cd ./submodules/masktextspotterv3/
python setup.py build develop
cd ../../

cd ./submodules/yoro/
python setup.py build
cd ../../
```

或是直接使用我們事先準備好的 Workspace Docker
Image，已建置完成的專案路徑位於 `/root/tomonejam`：

``` bash
docker pull zhengling/tomonejam:workspace
```

## Ensemble Inference

### 事前準備

在進行 Ensemble Inference 之前，請先準備以下必要項目：

1.  模型資料夾

    請依照下列目錄結構存放模型檔案：

         modeldir/
         ├── bbox_rec
         │   ├── bbox_rec.pth
         │   └── bbox_rec.py
         ├── det.zip
         ├── e2e
         │   ├── e2e.pth
         │   └── e2e.yaml
         └── rbox_rec
             ├── char_dict.txt
             └── inference
                 ├── inference.pdiparams
                 ├── inference.pdiparams.info
                 └── inference.pdmodel

         4 directories, 9 files

    其中 e2e, det, bbox_rec, rbox_rec 等名稱與可與
    [#此專案使用的開源模型](#此專案使用的開源模型) 對應

2.  影像資料夾

    將需要進行辨識的影像集中在一個資料夾之下，例如：

        private_data_v2/
        ├── image_1.jpg
        ├── image_2.jpg
        ├── image_3.jpg
        ├── ...

### 進行預測

執行 `./integration/ensemble_inference` 以進行 Ensemble Inference

    usage: ensemble_inference [-h] --imgdir IMGDIR --modeldir MODELDIR --workdir WORKDIR

    Ensemble inference

    optional arguments:
      -h, --help           show this help message and exit
      --imgdir IMGDIR      Image folder path for inference
      --modeldir MODELDIR  Model folder path
      --workdir WORKDIR    Working directory path

其中 `--modeldir` 與 `--imgdir`
請分別帶入先前說明的模型資料夾與影像資料夾，而 `--workdir`
則可帶入自訂的工作目錄，程式會將 Ensemble Inference
結果儲存為工作目錄下的 `ensemble.csv`。

執行範例如下：

``` bash
./integration/ensemble_inference    \
    --imgdir private_data_v2        \
    --modeldir modeldir             \
    --workdir workdir
```

##### 使用 Workspace Docker Image 進行預測

除了使用上述方式進行預測之外，也可以直接使用我們事先準備好的 Workspace
Docker 進行預測。

請依照說明事先準備好模型資料夾 (modeldir) 與影像資料夾
(imgdir)，並事先建立一個工作目錄 (workdir)，再使用以下指令進行 Ensemble
Inference：

``` bash
docker run -it --rm --gpus all                      \
    -v $(pwd)/modeldir:/root/modeldir               \
    -v $(pwd)/imgdir:/root/imgdir                   \
    -v $(pwd)/workdir:/root/workdir                 \
    --entrypoint /usr/bin/python                    \
    zhengling/tomonejam:workspace                   \
    /root/tomonejam/integration/ensemble_inference  \
    --imgdir /root/imgdir                           \
    --modeldir /root/modeldir                       \
    --workdir /root/workdir
```

## 2021/11/18 Update

新增相容於 \[‘sm_35’, ‘sm_50’, ‘sm_60’, ‘sm_61’, ‘sm_70’, ‘sm_75’,
‘sm_80’, ‘sm_86’\] GPU 硬體的 Docker Image：

``` bash
# Base Image
docker pull zhengling/tomonejam:base-cu114

# Workspace Image
docker pull zhengling/tomonejam:workspace-cu114
```

但我們無法保證所有硬體都能夠跑出一致的結果。  
如果需要重現我們的實驗結果，建議使用與我們相同的 GPU (NVIDIA RTX 2080)。

## 2021/11/27 Update

附加複賽簡報於 doc 資料夾。
